const mqtt = require("mqtt");
const url = process.env.MQTT_URL || "mqtt://localhost:1883";
const client = mqtt.connect(url);
const {data} = require("./routes/index");

client.on("connect", () => {
    client.subscribe("douze", (err) => {
        if (!err) {
            console.log("Connected to MQTT");
        }
    });
});

client.on("message", (topic, message) => {
    // message is Buffer


    console.log(message.toString());
    let dataReceived = JSON.parse(message.toString());
    if (topic === "douze") {
        console.log("Received data: ", dataReceived);
        data.temp = parseFloat(dataReceived.temp);
        data.humidity = parseFloat(dataReceived.humidity);
        data.last_updated = new Date();
    }
});


module.exports = client;


