var express = require('express');
var router = express.Router();

const data = {
    last_updated: new Date(),
    temp: -1,
    humidity: -1
};

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express', temp: data.temp, humidity: data.humidity, last_updated: data.last_updated});
});


router.post("/data", (req, res) => {
    let dataReceived = req.body;
    console.log("Received data: ", dataReceived);
    data.temp = parseFloat(dataReceived.temp);
    data.humidity = parseFloat(dataReceived.humidity);
    data.last_updated = new Date();
    res.send(data);
});

module.exports = {router, data}
